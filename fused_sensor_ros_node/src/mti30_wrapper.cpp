
//--------------------------------------------------------------------------------
// Mti-30 Feature test
//  -- based on Xsens public 'open-source' API.
//  -- (standard) SDK/API not working for ARM
//
// features:
// 1) parametered ctrl of 'orientation' output:
//    types:Quternion/RotationMatrix/EulerAngle..
//    frequency: 1~400 Hz
//
// 2) sync output to trigger camera (SyncOut)
//    frequency (frame rate): 1~400 Hz (fps)
//    pulse width
//
//--------------------------------------------------------------------------------
#include "mti30_wrapper.h"

Journaller* gJournal = 0;

template class MTI30Wrapper<std_msgs::String>;
template class MTI30Wrapper<geometry_msgs::Quaternion>;

template<typename OrientationType>
MTI30Wrapper<OrientationType>::MTI30Wrapper(ros::NodeHandle& n, uint16_t sampleFreq, uint16_t triggerFreq)
	: IMUWrapper<XsDevice, OrientationType>(n, sampleFreq, triggerFreq)
	, callback_(boost::bind(&MTI30Wrapper::publishPacket, this, _1))
{
	GenericPeripheral::advertise<OrientationType>(DEFAULT_IMU_PUBLISH_PATH);

	this->log(GenericLogger::INFO, "Creating XsControl object...");
	control_ = XsControl::construct();
	assert(control_ != 0);

	this->log(GenericLogger::INFO, "Scanning for devices...");
	portInfoArray_ = XsScanner::scanPorts();

	this->log(GenericLogger::INFO, "Initializing device...");
	initDevice();

	this->log(GenericLogger::INFO, "Configuring device...");
	configureDevice();
}

template<typename OrientationType>
MTI30Wrapper<OrientationType>::~MTI30Wrapper()
{
	std::cout << "Closing log file..." << std::endl;
	if (!this->device_->closeLogFile())
	{
		handleError("Failed to close log file. Aborting.");
		return;
	}

	std::cout << "Closing port..." << std::endl; 
	control_->closePort(mtPort_.portName().toStdString());

	std::cout << "Freeing XsControl object..." << std::endl;
	control_->destruct();

	std::cout << "Successful exit." << std::endl;
}

template<typename OrientationType>
int MTI30Wrapper<OrientationType>::handleError(std::string errorString)
{
	control_->destruct();
	std::cout << errorString << std::endl;
	return -1;
}

template<typename OrientationType>
void MTI30Wrapper<OrientationType>::initDevice(void)
{
	for (auto const& portInfo : portInfoArray_)
	{
		if (portInfo.deviceId().isMti() || portInfo.deviceId().isMtig())
		{
			mtPort_ = portInfo;
			break;
		}
	}

	if (mtPort_.empty())
	{
		handleError("No MTi device found. Aborting.");
		return;
	}

	std::cout << "Found a device with ID: " << mtPort_.deviceId().toString().toStdString()
		<< " @ port: " << mtPort_.portName().toStdString()
		<< ", baudrate: " << mtPort_.baudrate() << std::endl;

	std::cout << "Opening port..." << std::endl;
	if (!control_->openPort(mtPort_.portName().toStdString(), mtPort_.baudrate()))
	{
		handleError("Could not open port. Aborting.");
		return;
	}

	std::cout << "Getting device " << std::endl;// << std::to_string(mtPort_.deviceId) << std::endl;
	this->device_ = control_->device(mtPort_.deviceId());
	assert(device != 0);

	std::cout << "Device: " << this->device_->productCode().toStdString()
		<< ", with ID: " << this->device_->deviceId().toString()
		<< " opened." << std::endl;

	this->device_->addCallbackHandler(&callback_);
}

template<typename OrientationType>
void MTI30Wrapper<OrientationType>::configureDevice(void)
{
	// Put the device into configuration mode before configuring the device
	if (false == config(this->device_, this->sample_rate_, this->trigger_freq_))
	{
		handleError("Failed to config IMU device. Aborting.");
		return;
	}

	std::cout << "Creating a log file " << logFileName << "..." << std::endl;
	if (this->device_->createLogFile(logFileName) != XRV_OK)
	{
		handleError("Failed to create a log file. Aborting.");
		return;
	}
	else
		std::cout << "Created a log file: " << logFileName.c_str() << std::endl;

	std::cout << "Putting device into measurement mode..." << std::endl;
	if (!this->device_->gotoMeasurement())
	{
		handleError("Could not put device into measurement mode. Aborting.");
		return;
	}
}

template<typename OrientationType>
void MTI30Wrapper<OrientationType>::startRecording(void)
{
	std::cout << "Starting recording and ROS Loop..." << std::endl;
	if (!this->device_->startRecording())
	{
		handleError("Failed to start recording. Aborting.");
		return;
	}

	recordingStartTime_ = XsTime::timeStampNow();
}

template<typename OrientationType>
void MTI30Wrapper<OrientationType>::stopRecording(void)
{
	recordingEndTime_ = XsTime::timeStampNow();

	std::cout << "Stopping recording after " << std::to_string((recordingEndTime_- recordingStartTime_) / 1000.) << "s" << std::endl;
	if (!this->device_->stopRecording())
	{
		handleError("Failed to stop recording. Aborting.");
		return;
	}
}

template<typename OrientationType>
void MTI30Wrapper<OrientationType>::publishPacket(const XsDataPacket* packet)
{
	OrientationType q_out;
	xsDataPacketToOrientationType(packet, q_out);
	this->publish(q_out);
	return;
}

// config the IMU: output format (what 'fields' needed/measured, and syncOut/trigger to camera
template<typename OrientationType>
bool MTI30Wrapper<OrientationType>::config(XsDevice* device, uint16_t sampleFreq, uint16_t triggerFreq) {

	// Put the device into configuration mode before configuring the device
	std::cout << "Putting device into configuration mode..." << std::endl;
	if (!device->gotoConfig())
		return false; //failed

	std::cout << "Configuring the device..." << std::endl;

	// Important for Public XDA!
	// Call this function if you want to record a mtb file:
	device->readEmtsAndDeviceConfiguration();

	XsOutputConfigurationArray configArray;
	configArray.push_back(XsOutputConfiguration(XDI_PacketCounter, sampleFreq));
	//configArray.push_back(XsOutputConfiguration(XDI_SampleTimeCoarse, sampleFreq));
	configArray.push_back(XsOutputConfiguration(XDI_SampleTimeFine, sampleFreq));
	configArray.push_back(XsOutputConfiguration(XDI_StatusWord, sampleFreq));

	if (device->deviceId().isImu())
	{
		configArray.push_back(XsOutputConfiguration(XDI_DeltaV, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_DeltaQ, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_MagneticField, sampleFreq));
	}
	else if (device->deviceId().isVru() || device->deviceId().isAhrs())
	{
		configArray.push_back(XsOutputConfiguration(XDI_Quaternion, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_DeltaQ, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_DeltaV, sampleFreq));
	}
	else if (device->deviceId().isGnss())
	{
		configArray.push_back(XsOutputConfiguration(XDI_Quaternion, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_LatLon, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_AltitudeEllipsoid, sampleFreq));
		configArray.push_back(XsOutputConfiguration(XDI_VelocityXYZ, sampleFreq));
	}
	else
	{
		return false; //"Unknown device while configuring. Aborting.";
	}

	if (!device->setOutputConfiguration(configArray))
		return false; //"Could not configure MTi device. Aborting.";

	  //---------------------------------------------------------
	  // set the sync configuration
	uint16_t skipFactor = (400 / triggerFreq) - 1; //max otput fre of imu as 400 Hz

	XsSyncSettingArray syncSettingArray;
	XsSyncSetting syncSetting(XSL_Bi1Out,      /*XsSyncLine: SyncOut1*/
		XSF_IntervalTransitionMeasurement, /*XsSyncFunction*/
		XSP_RisingEdge,                    /*XsSyncPolarity*/
		1000, /*pulseWidth in uSec: /100uSec unit in mtibasedevice.cpp*/
		0,    /*offset*/
		0,              /*skipFirst*/
		skipFactor,     /*skipFactor*/
		0,    /*clockPeriod: mSec,only if using XSF_ClockBiasEstimation*/
		0     /*triggerOnce*/
	);

	syncSettingArray.push_back(syncSetting);

	if (!device->setSyncSettings(syncSettingArray))
		return false; //"Could not set Sync settings. Aborting.";

	return true; //config passed/successful
}