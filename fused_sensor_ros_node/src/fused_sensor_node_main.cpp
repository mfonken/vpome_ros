#include <ros/ros.h>
#include <unistd.h>
#include "fused_peripheral.h"

#define IMU_TYPE_MTI30
#define CAM_TYPE_AVT

#ifdef IMU_TYPE_MTI30
#include "mti30_wrapper.h"
#endif

#ifdef CAM_TYPE_AVT
#include "avt_vimba_camera_wrapper.h"
#endif

using namespace std;

int main(int argc, char* argv[])
{
	std::cout << "Starting..." << std::endl;
	ros::init(argc, argv, "FusedSensor");
	FusedPeripheral fusedSensor;

	/* Sensors */
#ifdef IMU_TYPE_MTI30
	fusedSensor.add<MTI30Wrapper<geometry_msgs::Quaternion>>("imu");
#elif IMU_TYPE_NONE
#warning "No IMU defined."
#else
#error "IMU type is undefined or invalid."
#endif

#ifdef CAM_TYPE_AVT
	fusedSensor.add<AvtVimbaCameraWrapper>("cam");
#elif CAM_TYPE_NONE
#warning "No camera defined."
#else
#error "Camera type is undefined or invalid."
#endif

	fusedSensor.start();

	return 0;
}