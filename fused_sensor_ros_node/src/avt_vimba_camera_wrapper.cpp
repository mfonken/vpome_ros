/// Copyright (c) 2014,
/// Systems, Robotics and Vision Group
/// University of the Balearic Islands
/// All rights reserved.
/// Note: This is a derivative of avt_vimba_camera-kinetic > mono_camera.cpp

#include "avt_vimba_camera_wrapper.h"

#define DEBUG_PRINTS 1

static double timestamp = 0.;

static double nowMs()
{
	return ros::Time::now().sec * 1000. + ros::Time::now().nsec / 1000000.;
}
static double msSince(double& start)
{
	double now = nowMs();
	double diff = now - start;
	start = now;
	return diff;
}
AvtVimbaCameraWrapper::AvtVimbaCameraWrapper(ros::NodeHandle& np) : CameraWrapper(np)
{
	// Prepare node handle for the camera
	// TODO use nodelets with getMTNodeHandle()

	// Start Vimba & list all available cameras
	api_.start();

	// Set the image publisher before the streaming
	advertise("image_raw");

	// Set the frame callback
	cam_.setCallback(boost::bind(&AvtVimbaCameraWrapper::frameCallback, this, _1));

	// Set the params
	param<std::string>("ip", ip_, std::string(""));
	param<std::string>("guid", guid_, std::string(""));
	param<std::string>("camera_info_url", camera_info_url_, std::string(""));
	std::string frame_id = param<std::string>("frame_id", std::string(""));
	param<bool>("show_debug_prints", show_debug_prints_, false);

	// Set camera info manager
	info_man_ = boost::shared_ptr<camera_info_manager::CameraInfoManager>(new camera_info_manager::CameraInfoManager(handle(), frame_id, camera_info_url_));

	// Start dynamic_reconfigure & run configure()
	reconfigure_server_.setCallback(boost::bind(&AvtVimbaCameraWrapper::configure, this, _1, _2));
}

AvtVimbaCameraWrapper::~AvtVimbaCameraWrapper(void) {
	this->cam_.stop();
}

void AvtVimbaCameraWrapper::frameCallback(const FramePtr& vimba_frame_ptr) {
	ros::Time ros_time = ros::Time::now();
	if (pub_.getNumSubscribers() > 0) {
		sensor_msgs::Image img;
		if (api_.frameToImage(vimba_frame_ptr, img)) {
			sensor_msgs::CameraInfo ci = info_man_->getCameraInfo();
			ci.header.stamp = img.header.stamp = ros_time;
			img.header.frame_id = ci.header.frame_id;
			publish(img, ci);
		}
		else {
			ROS_WARN_STREAM("Function frameToImage returned 0. No image published.");
		}
	}

	printf("FPS: %.3fHz\n", 1000. / msSince(timestamp));

	// updater_.update();
}

/** Dynamic reconfigure callback
*
*  Called immediately when callback first defined. Called again
*  when dynamic reconfigure starts or changes a parameter value.
*
*  @param newconfig new Config values
*  @param level bit-wise OR of reconfiguration levels for all
*               changed parameters (0xffffffff on initial call)
**/
void AvtVimbaCameraWrapper::configure(Config& newconfig, uint32_t level) {
	try {
		// resolve frame ID using tf_prefix parameter
		if (newconfig.frame_id == "") {
			newconfig.frame_id = "camera";
		}
		// The camera already stops & starts acquisition
		// so there's no problem on changing any feature.
		if (!cam_.isOpened()) {
			cam_.start(ip_, guid_, show_debug_prints_);
		}

		Config config = newconfig;
		this->cam_.updateConfig(newconfig);
		updateCameraInfo(config);
	}
	catch (const std::exception & e) {
		ROS_ERROR_STREAM("Error reconfiguring mono_camera node : " << e.what());
	}
}

void AvtVimbaCameraWrapper::updateCameraInfo(const avt_vimba_camera::AvtVimbaCameraConfig& config) {

	// Get camera_info from the manager
	sensor_msgs::CameraInfo ci = info_man_->getCameraInfo();

	// Set the frame id
	ci.header.frame_id = config.frame_id;

	// Set the operational parameters in CameraInfo (binning, ROI)
	int binning_or_decimation_x = std::max(config.binning_x, config.decimation_x);
	int binning_or_decimation_y = std::max(config.binning_y, config.decimation_y);

	// Set the operational parameters in CameraInfo (binning, ROI)
	ci.height = config.height;
	ci.width = config.width;
	ci.binning_x = binning_or_decimation_x;
	ci.binning_y = binning_or_decimation_y;

	// ROI in CameraInfo is in unbinned coordinates, need to scale up
	ci.roi.x_offset = config.roi_offset_x;
	ci.roi.y_offset = config.roi_offset_y;
	ci.roi.height = config.roi_height;
	ci.roi.width = config.roi_width;

	// set the new URL and load CameraInfo (if any) from it
	std::string camera_info_url;
	handle().getParam("camera_info_url", camera_info_url);
	if (camera_info_url != camera_info_url_) {
		info_man_->setCameraName(config.frame_id);
		if (info_man_->validateURL(camera_info_url)) {
			info_man_->loadCameraInfo(camera_info_url);
			ci = info_man_->getCameraInfo();
		}
		else {
			ROS_WARN_STREAM("Camera info URL not valid: " << camera_info_url);
		}
	}

	bool roiMatchesCalibration = (ci.height == config.roi_height
		&& ci.width == config.roi_width);
	bool resolutionMatchesCalibration = (ci.width == config.width
		&& ci.height == config.height);
	// check
	ci.roi.do_rectify = roiMatchesCalibration || resolutionMatchesCalibration;

	// push the changes to manager
	info_man_->setCameraInfo(ci);
}