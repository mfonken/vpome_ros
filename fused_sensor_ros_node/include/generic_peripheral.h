#ifndef GENERIC_PERIPHERAL_H
#define GENERIC_PERIPHERAL_H

#include <ros/ros.h>
#include <fstream>

#define NO_SAMPLE_RATE 0
#define DEFAULT_SAMPLE_RATE NO_SAMPLE_RATE
#define DEFAULT_PUBLISHER_QUEUE_SIZE 100

template<class T>
using generic_callback_f = std::function<void(T)>;

static std::string logFileName = "/home/v/Desktop/logfile.mtb";
static std::string defaultDebugDir = "/home/v/Desktop/fused_sensor_dbg/";

class GenericLogger {
public:
	typedef enum
	{
		FILE = -1,
		OFF = 0,
		IMPORTANT,
		NORMAL,
		INFO,
	} log_level_t;

	GenericLogger(log_level_t console_level = NORMAL, std::string file_name = "", std::string header = "")
		: console_level_(console_level)
	{
		if (file_name != "")
		{
			if (file_name.find("/") == std::string::npos)
				file_name = defaultDebugDir + file_name;
			if (file_name.find(".") == std::string::npos)
				file_name = file_name + ".csv";
			log_file.open(file_name);
			if (!log_file.is_open())
				std::cout << "Failed to open file: " << file_name << std::endl;
			else
			{
				log_file.close();
				log_file.open(file_name, std::ofstream::out | std::ofstream::app);
				if (header != "")
					log_file << header;
			}
		}
	}

	void setLogLevel(log_level_t level) { console_level_ = level; }

	void log(log_level_t level, std::string str)
	{
		if(level >= console_level_)
			std::cout << str << std::endl;
		else if (level == FILE && log_file.is_open())
			log_file << str << std::endl;
	}	
	
private:
	std::ofstream log_file;
	log_level_t console_level_;
};

class GenericPeripheral {
protected:
	ros::NodeHandle& n_;
	ros::Publisher pub_;

public:
	GenericPeripheral(ros::NodeHandle& n, uint16_t sample_rate = DEFAULT_SAMPLE_RATE, std::string logfile = "", uint32_t publish_queue_size = DEFAULT_PUBLISHER_QUEUE_SIZE)
		: n_(n), sample_rate_(sample_rate), publish_queue_size_(publish_queue_size)
	{}
	~GenericPeripheral()
	{
		this->pub_.shutdown();
	}

	ros::NodeHandle& handle() { return n_; }

	template<typename T>
	void advertise(std::string path)
	{
		printf("C\n");
		pub_ = n_.advertise<T>(path, publish_queue_size_);
	}

	template<typename T>
	void publish(T sample)
	{
		pub_.publish(sample);
	}

	template<typename T>
	T param(std::string name, T def)
	{
		T val;
		param<T>(name, val, def);
		return val;
	}

	template<typename T>
	void param(std::string name, T& val, T def)
	{
		n_.param(name.c_str(), val, def);
	}

	void log(GenericLogger::log_level_t level, const char * str)
	{
		log(level, std::string(str));
	}
	void log(GenericLogger::log_level_t level, std::string str)
	{
		logger_.log(level, str);
	}

	uint16_t sample_rate_;
	uint32_t publish_queue_size_;

	void setLogLevel(GenericLogger::log_level_t level) { logger_.setLogLevel(level); }

private:
	GenericLogger logger_;
};

#endif