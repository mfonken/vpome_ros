//--------------------------------------------------------------------------------
// Mti-30 Feature test
// mti30.h
//--------------------------------------------------------------------------------
#ifndef MTI30_H
#define MTI30_H

#include <xscontroller/xscontrol_def.h>
#include <xscontroller/xsdevice_def.h>
#include <xscontroller/xsscanner.h>
#include <xstypes/xsoutputconfigurationarray.h>
#include <xstypes/xssyncsettingarray.h>
#include <xstypes/xsdatapacket.h>
#include <xstypes/xstime.h>
#include <xscommon/xsens_mutex.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <list>
#include <string>
#include <thread>

#include <ros/ros.h>
#include "std_msgs/String.h"
#include "geometry_msgs/Quaternion.h"
#include <sstream>

#include "imu_wrapper.h"

#define DEFAULT_SAMPLE_FREQUENCY DEFAULT_IMU_FREQUENCY     //1~400
#define DEFAULT_TRIGGER_FREQUENCY DEFAULT_SAMPLE_FREQUENCY

class XsCallbackHandler : public XsCallback
{
public:
	XsCallbackHandler(generic_callback_f<const XsDataPacket*> hostCallback,
		size_t maxBufferSize = 5)
		: hostCallback_(hostCallback)
		, m_maxNumberOfPacketsInBuffer(maxBufferSize)
		, m_numberOfPacketsInBuffer(0)
	{}

	virtual ~XsCallbackHandler() throw() {}

	bool packetAvailable() const
	{
		xsens::Lock locky(&m_mutex);
		return m_numberOfPacketsInBuffer > 0;
	}

	XsDataPacket getNextPacket()
	{
		assert(packetAvailable());
		xsens::Lock locky(&m_mutex);
		XsDataPacket oldestPacket(m_packetBuffer.front());
		m_packetBuffer.pop_front();
		--m_numberOfPacketsInBuffer;
		return oldestPacket;
	}

protected:
	void onLiveDataAvailable(XsDevice*, const XsDataPacket* packet) override
	{
		xsens::Lock locky(&m_mutex);
		assert(packet != 0);
		while (m_numberOfPacketsInBuffer >= m_maxNumberOfPacketsInBuffer)
			(void)getNextPacket();

		m_packetBuffer.push_back(*packet);
		++m_numberOfPacketsInBuffer;
		assert(m_numberOfPacketsInBuffer <= m_maxNumberOfPacketsInBuffer);

		hostCallback_(packet);
	}
private:
	mutable xsens::Mutex m_mutex;

	size_t m_maxNumberOfPacketsInBuffer;
	size_t m_numberOfPacketsInBuffer;
	std::list<XsDataPacket> m_packetBuffer;

	generic_callback_f<const XsDataPacket*> hostCallback_;
};

template <typename OrientationType>
class MTI30Wrapper : public IMUWrapper<XsDevice, OrientationType>
{
	/* XsDataPacket to orientation type overloads */
	void xsDataPacketToOrientationType(const XsDataPacket* packet, std_msgs::String& out)
	{
		XsQuaternion q = packet->orientationQuaternion();
		out.data = std::to_string(q.w()) + "," + std::to_string(q.x()) + "," + std::to_string(q.y()) + "," + std::to_string(q.z());
	}

	void xsDataPacketToOrientationType(const XsDataPacket* packet, geometry_msgs::Quaternion& out)
	{
		XsQuaternion q = packet->orientationQuaternion();
		out.w = q.w();
		out.x = q.x();
		out.y = q.y();
		out.z = q.z();
	}

public:
	MTI30Wrapper(ros::NodeHandle& n, uint16_t sampleFreq = DEFAULT_SAMPLE_FREQUENCY, uint16_t triggerFreq = DEFAULT_TRIGGER_FREQUENCY);
	~MTI30Wrapper();
	int handleError(std::string errorString);
	void initDevice(void);
	void configureDevice(void);
	void startRecording(void);
	void stopRecording(void);
	void publishPacket(const XsDataPacket* packet);
	bool config(XsDevice* device, uint16_t sampleFreq, uint16_t triggerFreq);

	XsControl* control_ = NULL;
	XsPortInfoArray portInfoArray_;
	XsPortInfo mtPort_;
	XsCallbackHandler callback_;

	int64_t recordingStartTime_;
	int64_t recordingEndTime_;
};

#endif //MXI30_H
