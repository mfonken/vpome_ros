//--------------------------------------------------------------------------------
// Generic IMU wrapper for ROS
// imu_wrapper.h
//--------------------------------------------------------------------------------
#ifndef IMU_WRAPPER_H
#define IMU_WRAPPER_H

#include "generic_peripheral.h"

#include "std_msgs/String.h"
#include "geometry_msgs/Quaternion.h"
#include <sstream>

#define DEFUALT_IMU_PUBLISHER_QUEUE_SIZE 100
#define DEFAULT_IMU_FREQUENCY 20// hz
#define DEFAULT_IMU_PUBLISH_PATH "imu_samples"

#define TRIGGER_OFF 0

template <class DeviceType, class OrientationType>
class IMUWrapper : public GenericPeripheral
{
public:
	IMUWrapper(ros::NodeHandle& n, uint16_t sample_freq = DEFAULT_IMU_FREQUENCY, uint16_t trigger_freq = TRIGGER_OFF) 
		: GenericPeripheral(n, sample_freq), trigger_freq_(trigger_freq)
	{}

protected:
	bool hasDevice(void) { return (device_ != NULL); }

	uint16_t trigger_freq_;

	OrientationType sample_;
	DeviceType* device_ = NULL;
};

#endif
