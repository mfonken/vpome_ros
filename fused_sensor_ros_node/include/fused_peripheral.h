//--------------------------------------------------------------------------------
// Generic fused sensor for a ROS node
// fused_sensor.h
//--------------------------------------------------------------------------------

#ifndef FUSED_SENSOR_H
#define FUSED_PERIPHERAL_H

#include <vector>
#include <ros/ros.h>

#include "generic_peripheral.h"

class FusedPeripheral
{
	GenericLogger logger_;
	ros::NodeHandle n_;
	std::map<std::string, GenericPeripheral*> peripherals_;
public:
	FusedPeripheral() : n_("~")
	{}

	~FusedPeripheral()
	{
		for (auto& p : peripherals_)
			delete p.second;
	}

	template<typename T>
	void add(std::string name = "")
	{
		peripherals_[name] = new T(n_);
	}

	void print()
	{
		std::cout << "Peripherals:" << std::endl;
		for (auto& p : peripherals_)
			std::cout << "\t" << p.first << std::endl;
	}

	void setLogLevel(GenericLogger::log_level_t level)
	{
		for (auto& p : peripherals_)
			p.second->setLogLevel(level); 
	}
	void start(void)
	{
		print();
		ros::spin();
	}
};

#endif
