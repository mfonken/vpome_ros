//--------------------------------------------------------------------------------
// Generic camera wrapper for ROS
// camera_wrapper.h
//--------------------------------------------------------------------------------

#ifndef CAMERA_WRAPPER_H
#define CAMERA_WRAPPER_H

#include "generic_peripheral.h"

template <class CameraType>
class CameraWrapper : public GenericPeripheral
{
public:
	CameraWrapper(ros::NodeHandle& n) : GenericPeripheral(n), it_(n), cam_(ros::this_node::getName())
	{}

	~CameraWrapper()
	{
		pub_.shutdown();
	}

	void publish(sensor_msgs::Image image, sensor_msgs::CameraInfo info)
	{
		pub_.publish(image, info);
	}

	void advertise(std::string path)
	{
		pub_ = it_.advertiseCamera(path, 1);
	}

protected:
	CameraType cam_;

	image_transport::ImageTransport it_;
	image_transport::CameraPublisher pub_;
};

#endif
