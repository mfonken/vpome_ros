/// Copyright (c) 2014,
/// Systems, Robotics and Vision Group
/// University of the Balearic Islands
/// All rights reserved.
/// Note: This is a derivative of avt_vimba_camera-kinetic > mono_camera.h

#ifndef AVT_VIMBA_CAMERA_WRAPPER_H
#define AVT_VIMBA_CAMERA_WRAPPER_H

#include <avt_vimba_camera.h>
#include <AvtVimbaCameraConfig.h>
#include <avt_vimba_api.h>

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <camera_info_manager/camera_info_manager.h>
#include <image_transport/image_transport.h>
#include <dynamic_reconfigure/server.h>

#include <string>

#include "camera_wrapper.h"

class AvtVimbaCameraWrapper : public CameraWrapper<avt_vimba_camera::AvtVimbaCamera> {
public:
	AvtVimbaCameraWrapper(ros::NodeHandle& np);
	~AvtVimbaCameraWrapper(void);

private:
	avt_vimba_camera::AvtVimbaApi api_;

	std::string ip_;
	std::string guid_;
	std::string camera_info_url_;
	bool show_debug_prints_;

	// sensor_msgs::CameraInfo left_info_;
	boost::shared_ptr<camera_info_manager::CameraInfoManager> info_man_;

	// Dynamic reconfigure
	typedef avt_vimba_camera::AvtVimbaCameraConfig Config;
	typedef dynamic_reconfigure::Server<Config> ReconfigureServer;
	ReconfigureServer reconfigure_server_;

	// Camera configuration
	Config camera_config_;

	void frameCallback(const FramePtr& vimba_frame_ptr);
	void configure(Config& newconfig, uint32_t level);
	void updateCameraInfo(const Config& config);
};

#endif
