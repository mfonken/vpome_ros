#ifndef PROPAGATION_TEST_H
#define PROPAGATION_TEST_H

#include "ROIManager.h"
#include "ImageSubscriber.h"

#include <vector>

#include <xscontroller/xscontrol_def.h>
#include <xscontroller/xsdevice_def.h>
#include <xscontroller/xsscanner.h>
#include <xstypes/xsoutputconfigurationarray.h>
#include <xstypes/xssyncsettingarray.h>
#include <xstypes/xsdatapacket.h>
#include <xstypes/xstime.h>
#include <xscommon/xsens_mutex.h>

#include "geometry_msgs/Quaternion.h"

#define MAX_PROPAGATION_LENGTH 100

#define PIXEL_WIDTH		3.45e-6 // m
#define FOCAL_LENGTH	10e-3	// m
#define FOCAL_LENGTH_PX ( FOCAL_LENGTH / PIXEL_WIDTH )

#define SUBSCRIBER_QUEUE_SIZE 1
//#define DEBUG_OUT

#define ROI_TILE_SIZE 36

static std::string defaultSubscriberName = "/fused_sensor_node/imu_samples";

static XsEuler DEFAULT_BASE_ROTATION(-90, 180, 0);

typedef struct
{
	double x, y;
	double padding_x;
	double padding_y;
} point_t;

typedef struct
{
	double i, j;
	point_t origin;
} translation_t;

typedef struct
{
	double i, j, k;
} vec3_t;

typedef struct
{
	point_t curr;
	point_t prev;
	uint32_t propagations;
	void update(point_t& new_point)
	{
		prev = curr;
		curr = new_point;
		propagations++;
	}
	translation_t translation(void) const
	{
		return (translation_t) { curr.x - prev.x, curr.y - prev.y, prev };
	}
} propagated_point_t;

class FocalFrame {
public:
	FocalFrame(uint32_t width = 0, uint32_t height = 0, uint32_t focal_length = 0);
	~FocalFrame() {}

	bool initialized = false;
	double width_;
	double height_;
	XsVector3 focal_point_;
	XsVector3 unit_orientation_;
	XsQuaternion H_;
	XsQuaternion base_rotation_;
	double focal_rect_dot_product_;
	propagated_point_t zero_point_;
	std::vector<propagated_point_t> points_;

	void initialize(uint32_t width, uint32_t height, uint32_t focal_length);
	void resetOrientation(XsQuaternion& q);
	void addPoint(double x, double y, double padding_x, double padding_y);
	void addPoint(point_t& p);
	void updatePoint(int i, point_t& p);
	point_t rotatePointVectorByQuaternion(point_t& point, XsQuaternion& q);

	cv::Rect getPointsMinMax(void);
	size_t numPoints(void) { return points_.size(); }
	void resetPoints(void) { points_.clear(); }

	/* Quaternion operations */
	static XsVector3 rotateVectorByQuaternion(const XsVector3& v, const XsQuaternion& q);
	static XsVector3 getQuaternionUnitVector(const XsQuaternion& q);
	static XsQuaternion quaternionDiff(const XsQuaternion& a, const XsQuaternion& b);

	void printPoint(point_t& p, std::string name) { printPoint(p, name.c_str()); }
	void printPoint(point_t& p, const char* name);
	void printXsQuaternion(XsQuaternion& q, const char* name);
	void printXsVector3(XsVector3& v, const char* name);
};

class PropagationManager {
public:
	PropagationManager(ros::NodeHandle& n, ros::NodeHandle& np, uint32_t width = 0, uint32_t height = 0, uint32_t focal_length = 0);
	void initializeFrame(uint32_t width, uint32_t height, uint32_t focal_length = FOCAL_LENGTH_PX)
	{
		focal_frame_.initialize(width, height, focal_length);
		roi_manager_.initialize(width, height);
	}
	FocalFrame focal_frame_;
	ROIManager roi_manager_;

	ros::NodeHandle& n_;
	ros::NodeHandle& np_;
	ros::Subscriber imu_sub_;

	XsQuaternion q_, q_prev_;
	XsQuaternion q_sample_;
	void drawPointsOnCVMat(cv::Mat& M) const;
	void imuCallback(const geometry_msgs::Quaternion& q);
	void updateROI(cv::Point& offset);
	cv::Mat getROIFromMat(cv::Mat& M);
	void propagate(XsQuaternion& q_new, cv::Mat& M);
	translation_t propagatePointOnSurfaceByQuaternionDelta(propagated_point_t& p, XsQuaternion& q_delta);
	void propagatePointOnSurfaceByReferenceTranslation(propagated_point_t& p, translation_t& T);
	void updateQuaternion(XsQuaternion& q_new, XsQuaternion& q_delta_out);
};

#endif
