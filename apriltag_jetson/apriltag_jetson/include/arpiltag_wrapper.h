#ifndef APRILTAG_WRAPPER_H
#define APRILTAG_WRAPPER_H

#include "ImageSubscriber.h"
#include "PropagationManager.h"

#include <vector>
#include <map>
#include <string>
#include <math.h>
#include <future>
#include <stdio.h>
#include <fstream>

extern "C" {
#include "apriltag.h"
#include "tag36h11.h"
#include "tag25h9.h"
#include "tag16h5.h"
#include "tagCircle21h7.h"
#include "tagCircle49h12.h"
#include "tagCustom48h12.h"
#include "tagStandard41h12.h"
#include "tagStandard52h13.h"
#include "common/getopt.h"
}

#define CONSOLE_DEBUG
#define DISPLAY_FRAME
#define USE_WORKER_THREAD

#ifndef USE_WORKER_THREAD
static void fakeWorker(void) {}
#endif

#define PACKAGE_PATH				"/apriltag_wrapper/"
#define DEFAULT_DEBUG_FILE_NAME		"/home/v/Desktop/apriltag_dbg/perf.csv"
#define DEFAULT_IMAGE_SUB_PATH		PACKAGE_PATH "image_transport_path"
#define DEFAULT_FRAME_WIDTH_BASE	2464
#define DEFAULT_FRAME_HEIGHT_BASE	2056
#define DEFAULT_DISPLAY_FACTOR		1
#define DEFAULT_DISPLAY_WIDTH		(DEFAULT_FRAME_WIDTH_BASE  >> DEFAULT_DISPLAY_FACTOR)
#define DEFAULT_DISPLAY_HEIGHT		(DEFAULT_FRAME_HEIGHT_BASE >> DEFAULT_DISPLAY_FACTOR)

#define MIN_DETECTION_SIDE 20
#define DEFAULT_MAX_TRANSITION_DELTA 25

#define MAX_TARGETS 12
#define MIN_TARGETS 1

template<typename R>
bool is_ready(std::future<R> const& f)
{
	try { return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready; }
	catch (const std::future_error & e) { return true; }
}

typedef std::pair<double, std::string> timestep_t;
typedef std::vector<timestep_t> timestep_v;
struct detection_t
{
	apriltag_detection_t apriltag;
	double width, height, jitter;
	detection_t(apriltag_detection_t& apriltag, double jitter = DEFAULT_MAX_TRANSITION_DELTA)
	{
		this->apriltag = apriltag;
		this->jitter = jitter;

		double x, y,
			min_x = DEFAULT_FRAME_WIDTH_BASE, max_x = 0.,
			min_y = DEFAULT_FRAME_HEIGHT_BASE, max_y = 0.;
		for (uint8_t i = 0; i < 4; i++)
		{
			x = apriltag.p[i][0];
			y = apriltag.p[i][1];
			if (x < min_x) min_x = x;
			if (x > max_x) max_x = x;
			if (y < min_y) min_y = y;
			if (y > max_y) max_y = y;
		}
		if (max_x <= min_x) width = 0;
		else width = max_x - min_x;
		if (max_y <= min_y) height = 0;
		else height = max_y - min_y;
	}
};
static bool detectionSort(detection_t i, detection_t j) 
{ 
	double max_dim_i = MAX(i.width, i.height),
		max_dim_j = MAX(j.width, j.height);
	return (max_dim_i > max_dim_j); }
typedef std::vector<detection_t> detection_v;

class BasicStatistic {
public:
	BasicStatistic(int max_n = 0) : max_n_(max_n) {}
	void add(double v)
	{
		if (++n_ == 1)
		{
			avg_ = v;
			return;
		}
		double prev_avg = avg_;
		avg_ += (v - avg_) / (double(n_));
		if (max_n_ > 0 && n_ > max_n_) n_ = max_n_;
		var_ += (v - prev_avg) * (v - avg_);
	}
	double avg(void) const { return avg_; }
	double stdv(void) const { return sqrt(var_ / (n_ + 1)); }
private:
	double avg_ = 0.;
	double var_ = 0.;
	int n_ = 0;
	int max_n_ = 0;
};


class AprilTagWrapper {
public:
	AprilTagWrapper(ros::NodeHandle& nh, ros::NodeHandle& nhp);
	~AprilTagWrapper() {}
	void end(void);

	class AprilTagDetectorConfig {
	public:
		apriltag_family_t* tf_ = NULL;
		void setFamily();
		void applyConfigToDetector(apriltag_detector_t* td);
		std::string ToString() const;

		std::string family_name_;
		double quad_decimate_ = 4.0;
		double quad_sigma_ = 0.0;
		int nthreads_ = 1;
		bool debug_ = true;
		bool refine_edges_ = true;
		int num_frames_for_test_;
		double expected_frame_rate_hz_;
	};

	class AprilTagDetectionManager {
	public:
		AprilTagDetectionManager(ros::NodeHandle& nh, ros::NodeHandle& nhp, double max_transition_delta = DEFAULT_MAX_TRANSITION_DELTA) 
			: nh_(nh), nhp_(nhp), max_transition_delta_(max_transition_delta), propagation_mgr_(nh, nhp), subframe_offset_(0,0) {}
		~AprilTagDetectionManager()
		{
			apriltag_detections_destroy(detections_);
		}
		detection_v get(void) const { return tracked_detections_; }
		apriltag_detection_t offsetDetection(apriltag_detection_t* det) const;
		int update(zarray_t* new_detections);
		point_t getCenter(int i) const;
		double getJitter(int i) const;

		void print(void);

		PropagationManager propagation_mgr_;
		cv::Point subframe_offset_;
	private:
		ros::NodeHandle nh_;
		ros::NodeHandle nhp_;
		double max_transition_delta_;
		zarray_t* detections_ = NULL;
		detection_v tracked_detections_;

		double distance(double a[2], double b[2]);
		double compareDetections(apriltag_detection_t* a, apriltag_detection_t* b);
	};

	void testCallback(void);
	void update(void);
	std::string getConfigString(void) const { return config_.ToString(); }

	ImageSubscriber image_sub_;
	std::future<void> update_future;

	// Basic
	int ticks_ = 0;
	int dropped_frames_ = 0;
	int ignored_frames_ = 0;
	int processed_frames_ = 0;
	double process_rate_hz_ = 0.;
	double timestamp_ = 0.;
	int proc_width = 0;
	int proc_height = 0;
	double decimate_factor_ = 1.;
	int num_detections_ = 0;

	// Statistics/Profiling
	BasicStatistic detection_stat;
	BasicStatistic detection_time;
	BasicStatistic fps_stat;
	BasicStatistic tracking_v_[MAX_TARGETS];
	std::map<std::string, timestep_v> timesteps_;

	// Detection
	apriltag_detector_t* td_ = NULL;
	AprilTagDetectorConfig config_;
	cv::Mat frameMat_, display_frame_;
	AprilTagDetectionManager detection_manager_;

	class Debug {
		const AprilTagWrapper* host_;
	public:
		Debug(AprilTagWrapper* host);
		~Debug();
		void frameInfo(void);
		void timesteps(void);
		void detections(cv::Mat& frame);
		void propagations(cv::Mat& frame);
		void display(cv::Mat& frame);
		void writeLine(std::string line, bool perf = true);

		std::ofstream outfile;
		std::string outfile_name = "";
		int outfile_tick = 0;

		std::ofstream perffile;
	};
	Debug debug;

private:
	ros::NodeHandle nh_;
	ros::NodeHandle nhp_;
};
#endif