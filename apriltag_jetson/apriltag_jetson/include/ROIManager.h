#ifndef ROI_MANAGER_H
#define ROI_MANAGER_H

#include "ImageSubscriber.h"

#define ROI_PADDING_FACTOR	150 // Pixels added padding
#define ROI_RESET_FACTOR	0.01 // Percent grow ROI on reset tick
#define MAX_RESET_TRY_MS	300

class ROIManager
{
public:
	ROIManager(uint32_t width = 0, uint32_t height = 0);

	double last_i_;
	double last_j_;
	double reset_timer_;

	void initialize(uint32_t width, uint32_t height);
	void reset(cv::Point& offset);
	void limitBoundaryDimension(int& x, int& w, int W);
	void update(cv::Rect& point_limits, cv::Point& offset);
	cv::Rect getROI(void) const { return roi_; }
	cv::Rect getPrevROI(void) const { return prev_roi_; }

private:
	bool initialized = false;
	cv::Rect roi_, prev_roi_;
	cv::Size frame_size_;
};

#endif
