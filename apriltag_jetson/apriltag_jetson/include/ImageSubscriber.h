#ifndef IMAGE_SUBSCRIBER_H
#define IMAGE_SUBSCRIBER_H

#include <stdio.h>

#include <ros/ros.h>
#include "std_msgs/String.h"
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

static const char* DEFAULT_IMAGE_TRANSPORT_PATH = "/fused_sensor_node/image_raw";

static double nowMs()
{
	return ros::Time::now().sec * 1000. + ros::Time::now().nsec / 1000000.;
}
static double msSince(double& start)
{
	double now = nowMs();
	double diff = now - start;
	start = now;
	return diff;
}
static bool isTimedOut(double start, double max_time)
{
	return (nowMs() - start) > max_time;
}

class ImageSubscriber {
public:
	ImageSubscriber(ros::NodeHandle& nh, ros::NodeHandle& nhp, std::function<void(void)> hostCallback, const char* pathParameter);
	~ImageSubscriber(void);

	void callback(const sensor_msgs::ImageConstPtr& msg);

	const void getImageMat(cv::Mat& M) {
		M = (image_ptr_->image).clone();
	}
	double getLastTransportMs(void) { return last_transport_ms; }
	double getFrameRateHz(void) { return frame_rate_hz; }
private:
	ros::NodeHandle nh_;
	ros::NodeHandle nhp_;

	const char* host_node_path_parameter_name_;

	image_transport::ImageTransport it_;
	image_transport::Subscriber sub_;

	//Config camera_config_;

	cv_bridge::CvImageConstPtr image_ptr_;
	std::function<void(void)> hostCallback_;
	std::string image_transport_path_;

	double last_transport_ms;
	double frame_rate_hz;
	double timestamp = 0;
};

#endif
