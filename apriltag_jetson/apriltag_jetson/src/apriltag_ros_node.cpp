#include "arpiltag_wrapper.h"

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
	printf("Starting.\n");
	ros::init(argc, argv, "apriltag_ros_node");

	ros::NodeHandle nh;
	ros::NodeHandle nhp("~");
	
	printf("Initializing.\n");
	cv::namedWindow("Frame");
	cv::startWindowThread();
	AprilTagWrapper detector(nh, nhp);

	printf("Ready... (Waiting for frame)\n");
	ros::spin();


	printf("Destroying frame.\n");
	cv::destroyWindow("Frame");

    return 0;
}