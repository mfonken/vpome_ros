#include "ImageSubscriber.h"

void ImageSubscriber::callback(const sensor_msgs::ImageConstPtr& msg)
{
	try
	{
		double transport_start_ms = msg->header.stamp.sec * 1000. + msg->header.stamp.nsec / 1000000.;
		image_ptr_ = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);

		last_transport_ms = msSince(transport_start_ms);
		frame_rate_hz = 1000. / (msSince(timestamp));
	}
	catch (cv_bridge::Exception & e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
		return;
	}
	hostCallback_();
}

ImageSubscriber::ImageSubscriber(ros::NodeHandle& nh, ros::NodeHandle& nhp, std::function<void(void)> hostCallback, const char* pathParameter)
	: nh_(nh), nhp_(nhp), it_(nhp), hostCallback_(hostCallback), host_node_path_parameter_name_(pathParameter)
{
	nhp_.param(host_node_path_parameter_name_, image_transport_path_, std::string(DEFAULT_IMAGE_TRANSPORT_PATH));
	std::cout << "Subscribing to image: " << image_transport_path_ << std::endl;
	sub_ = it_.subscribe(image_transport_path_, 1, boost::bind(&ImageSubscriber::callback, this, _1));
}
ImageSubscriber::~ImageSubscriber(void) {
	std::cout << "Ending..." << std::endl;
}