#include "PropagationManager.h"

static cv::Scalar red(0, 0, 255);
static cv::Scalar purple(204, 50, 153);
static cv::Scalar yellow(0, 255, 255);
static cv::Scalar light_gray(220, 220, 220);
static cv::Scalar light_blue(218, 197, 134);

PropagationManager::PropagationManager(
	ros::NodeHandle& n, ros::NodeHandle& np,
	uint32_t width, uint32_t height, uint32_t focal_length)
	: n_(n), np_(np)
	, focal_frame_(width, height, focal_length)
	, roi_manager_(width, height)
{
	printf("Subscribing to %s.\n", defaultSubscriberName.c_str());

	imu_sub_ = np_.subscribe(defaultSubscriberName, SUBSCRIBER_QUEUE_SIZE, &PropagationManager::imuCallback, this);
}

void PropagationManager::drawPointsOnCVMat(cv::Mat& M) const
{
	double x_scale = focal_frame_.width_ / M.cols;
	double y_scale = focal_frame_.height_ / M.rows;

	int i = 0;
	translation_t T;
	double min_x = focal_frame_.width_ * x_scale, max_x = 0., min_y = focal_frame_.height_ * y_scale, max_y = 0.;
	if (!focal_frame_.points_.empty()) T = focal_frame_.points_[0].translation();

	cv::Rect roi = roi_manager_.getROI();
	cv::Rect prev_roi = roi_manager_.getPrevROI();

	double prev_x = prev_roi.x * x_scale;
	double prev_y = prev_roi.y * y_scale;
	double prev_w = prev_roi.width * x_scale;
	double prev_h = prev_roi.height * y_scale;
	double curr_x = roi.x * x_scale;
	double curr_y = roi.y * y_scale;
	double curr_w = roi.width * x_scale;
	double curr_h = roi.height * y_scale;

	cv::Point A(curr_x, curr_y);
	cv::Point B(curr_x + curr_w, curr_y);
	cv::Point C(curr_x, curr_y + curr_h);
	cv::Point D(curr_x + curr_w, curr_y + curr_h);

	if (fabs(curr_w - prev_w) < MAX_PROPAGATION_LENGTH)
	{
		cv::Point a(prev_x, prev_y);
		cv::Point b(prev_x + prev_w, prev_y);
		cv::Point c(prev_x, prev_y + prev_h);
		cv::Point d(prev_x + prev_w, prev_y + prev_h);
		cv::circle(M, a, 3, yellow, -1);
		cv::circle(M, b, 3, yellow, -1);
		cv::circle(M, c, 3, yellow, -1);
		cv::circle(M, d, 3, yellow, -1);
		cv::line(M, a, A, light_blue, 1);
		cv::line(M, b, B, light_blue, 1);
		cv::line(M, c, C, light_blue, 1);
		cv::line(M, d, D, light_blue, 1);
	}
	cv::rectangle(M, cv::Rect(curr_x, curr_y, curr_w, curr_h), light_blue);
	cv::circle(M, A, 5, red, -1);
	cv::circle(M, B, 5, red, -1);
	cv::circle(M, C, 5, red, -1);
	cv::circle(M, D, 5, red, -1);
	cv::circle(M, cv::Point(focal_frame_.zero_point_.curr.x, focal_frame_.zero_point_.curr.y), 8, light_blue, -1);
}

void PropagationManager::imuCallback(const geometry_msgs::Quaternion& q)
{
	XsQuaternion q_new(q.w, q.x, q.y, q.z);
	q_sample_ = q_new;
}

void PropagationManager::updateROI(cv::Point& offset)
{
	cv::Rect pointBoundary = focal_frame_.getPointsMinMax();
	roi_manager_.update(pointBoundary, offset);
}

cv::Mat PropagationManager::getROIFromMat(cv::Mat& M)
{
	cv::Rect roi = roi_manager_.getROI();
	//printf("ROI: (%d, %d) %dx%d\n", roi.x, roi.y, roi.width, roi.height);
	if (roi.width == 0 || roi.height == 0)
	{
		printf("Invalid ROI:(%d, %d) %dx%d\n", roi.x, roi.y, roi.width, roi.height);
		return M;
	}
	return M(roi);
}

void PropagationManager::propagate(XsQuaternion& q_new, cv::Mat& M)
{
	if (!focal_frame_.initialized) return;

	if (focal_frame_.H_.empty())
	{
		focal_frame_.resetOrientation(q_new);
		return;
	}
	XsQuaternion q_new_rot = focal_frame_.base_rotation_ * q_new;

	XsQuaternion q_delta;
	updateQuaternion(q_new_rot, q_delta);

#ifdef DEBUG_OUT
	focal_frame_.printXsQuaternion(q_delta, "Q_delta");
#endif

	translation_t T;
	int i = 0;
	for (propagated_point_t& p : focal_frame_.points_)
	{
#ifdef DEBUG_OUT
		std::cout << "Rotating point "; focal_frame_.printPoint(p.curr, std::string("P") + std::to_string(i));
#endif
		if (!i) T = propagatePointOnSurfaceByQuaternionDelta(p, q_delta);
		else propagatePointOnSurfaceByReferenceTranslation(p, T);

		i++;
	}

	if (focal_frame_.points_.size() > 0)
	{
		propagatePointOnSurfaceByReferenceTranslation(focal_frame_.zero_point_, T);
		roi_manager_.last_i_ = T.i; roi_manager_.last_j_ = T.j;
	}

	//printf("T(%.2f, %.2f)\n", T.i, T.j);

	drawPointsOnCVMat(M);
#ifdef DEBUG_OUT
	std::cout << std::endl;
#endif
}

translation_t PropagationManager::propagatePointOnSurfaceByQuaternionDelta(propagated_point_t& p, XsQuaternion& q_delta)
{
	point_t new_point = focal_frame_.rotatePointVectorByQuaternion(p.curr, q_delta);
	p.update(new_point);
	return p.translation();
}

void PropagationManager::propagatePointOnSurfaceByReferenceTranslation(propagated_point_t& p, translation_t& T)
{
	point_t new_point = { p.curr.x + T.i, p.curr.y + T.j };
	p.update(new_point);
}

void PropagationManager::updateQuaternion(XsQuaternion& q_new, XsQuaternion& q_delta)
{
	q_delta = focal_frame_.quaternionDiff(q_new, q_);
	q_ = q_new;
}

FocalFrame::FocalFrame(uint32_t width, uint32_t height, uint32_t focal_length)
	: width_((double)width), height_((double)height), focal_point_(width_ / 2, height_ / 2, (double)focal_length)
	, base_rotation_(DEFAULT_BASE_ROTATION)
{
	if (width > 0 & height > 0 && focal_length > 0) initialized = true;
}

void FocalFrame::initialize(uint32_t width, uint32_t height, uint32_t focal_length)
{
	if (width > 0 & height > 0 && focal_length > 0)
	{
		width_ = width;
		height_ = height;
		focal_point_ = XsVector3(width_ / 2, height_ / 2, (double)focal_length);
		initialized = true;

		zero_point_.curr = (point_t){ width_ / 2, height_ / 2, 0,0 };
	}
}

void FocalFrame::resetOrientation(XsQuaternion& q)
{
#ifdef DEBUG_OUT
	std::cout << "Resetting orientation to " << std::endl;
	printXsQuaternion(q, "H");
#endif

	unit_orientation_ = getQuaternionUnitVector(q);
	focal_rect_dot_product_ = focal_point_.dotProduct(unit_orientation_);

	H_ = q;
}

void FocalFrame::addPoint(double x, double y, double padding_x, double padding_y)
{
	point_t p = { x, y, padding_x, padding_y };
	addPoint(p);
}

void FocalFrame::addPoint(point_t& p)
{
	//printf("Adding point: (%d, %d)[%d, %d]\n", (int)p.x, (int)p.y, (int)p.padding_x, (int)p.padding_y);
	points_.push_back((propagated_point_t) { p, p, 0 });
}

void FocalFrame::updatePoint(int i, point_t& p)
{		
	points_[i].prev = points_[i].curr;
	points_[i].curr = p;
}

cv::Rect FocalFrame::getPointsMinMax(void)
{
	double min_x = width_, max_x = 0., min_y = height_, max_y = 0., curr_half_padding = 0., curr = 0;
	for (const propagated_point_t& p : points_)
	{
		curr_half_padding = p.curr.padding_x / 2;
		curr = p.curr.x - curr_half_padding;
		if (curr < min_x) min_x = curr;
		curr = p.curr.x + curr_half_padding;
		if (curr > max_x) max_x = curr;

		curr_half_padding = p.curr.padding_y / 2;
		curr = p.curr.y - curr_half_padding;
		if (curr < min_y) min_y = curr;
		curr = p.curr.y + curr_half_padding;
		if (curr > max_y) max_y = curr;
	}
	if (min_x > max_x) min_x = max_x;
	if (min_y > max_y) min_y = max_y;
	if (min_x < 0) min_x = 0;
	if (min_y < 0) min_y = 0;
	if (max_x > width_) max_x = width_;
	if (max_y > height_) max_y = height_;

	double width = max_x - min_x;
	double height = max_y - min_y;

	if (width == 0 && min_x == 0) width = width_;
	if (height == 0 && min_y == 0) height = height_;

	//printf("\tW:%d | H:%d\n", (int)width, (int)height);
	return cv::Rect(min_x, min_y, width, height);
}

point_t FocalFrame::rotatePointVectorByQuaternion(point_t& point, XsQuaternion& q)
{
	XsVector3 p_vec(point.x - focal_point_[0], point.y - focal_point_[1], focal_point_[2]);
	XsVector3 p_vec_rot = rotateVectorByQuaternion(p_vec, q);
	double scale_factor = focal_point_[2] / p_vec_rot[2];
	XsVector3 p_vec_rot_scaled = scale_factor * p_vec_rot;
	point_t new_point = { p_vec_rot_scaled[0] + focal_point_[0], p_vec_rot_scaled[1] + focal_point_[1] };

#ifdef DEBUG_OUT
	printXsVector3(p_vec, "Point vec");
	printXsVector3(p_vec_rot, "Rotated vec");
	std::cout << "Scale factor: " << std::to_string(scale_factor) << std::endl;
	printXsVector3(p_vec_rot_scaled, "Scaled vec");
	printPoint(new_point, "New point");
#endif

	return new_point;
}

XsVector3 FocalFrame::rotateVectorByQuaternion(const XsVector3& v, const XsQuaternion& q)
{
	double num12 = q.x() + q.x();
	double num2 = q.y() + q.y();
	double num = q.z() + q.z();
	double num11 = q.w() * num12;
	double num10 = q.w() * num2;
	double num9 = q.w() * num;
	double num8 = q.x() * num12;
	double num7 = q.x() * num2;
	double num6 = q.x() * num;
	double num5 = q.y() * num2;
	double num4 = q.y() * num;
	double num3 = q.z() * num;
	double num15 = ((v[0] * ((1. - num5) - num3)) + (v[1] * (num7 - num9))) + (v[2] * (num6 + num10));
	double num14 = ((v[0] * (num7 + num9)) + (v[1] * ((1. - num8) - num3))) + (v[2] * (num4 - num11));
	double num13 = ((v[0] * (num6 - num10)) + (v[1] * (num4 + num11))) + (v[2] * ((1. - num8) - num5));
	return XsVector3(num15, num14, num13);
}

XsVector3 FocalFrame::getQuaternionUnitVector(const XsQuaternion& q)
{
	double num = q.y() + q.y();
	double num2 = q.z() + q.z();
	double num3 = q.w() * num;
	double num4 = q.w() * num2;
	double num5 = q.x() * num;
	double num6 = q.x() * num2;
	double num7 = q.y() * num;
	double num8 = q.z() * num2;
	double num9 = ((1. - num7) - num8);
	double num10 = num5 + num4;
	double num11 = num6 - num3;
	return XsVector3(num9, num10, num11);
}

XsQuaternion FocalFrame::quaternionDiff(const XsQuaternion& a, const XsQuaternion& b)
{
	return a * b.inverse();
}

void FocalFrame::printPoint(point_t& p, const char* name)
{
	std::cout << std::string(name) << ":("
		<< p.x << ", "
		<< p.y << ")" << std::endl;
}

void FocalFrame::printXsQuaternion(XsQuaternion& q, const char* name)
{
	std::cout << std::string(name) << ":<"
		<< q.w() << ", "
		<< q.x() << ", "
		<< q.y() << ", "
		<< q.z() << ">" << std::endl;
}

void FocalFrame::printXsVector3(XsVector3& v, const char* name)
{
	std::cout << std::string(name) << ":("
		<< v[0] << ", "
		<< v[1] << ", "
		<< v[2] << ")" << std::endl;
}
