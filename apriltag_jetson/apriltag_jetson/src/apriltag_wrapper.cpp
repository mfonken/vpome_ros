#include "arpiltag_wrapper.h"
#include <sstream>

AprilTagWrapper::AprilTagWrapper(ros::NodeHandle& nh, ros::NodeHandle& nhp)
	: detection_manager_(nh, nhp)
	, image_sub_(nh, nhp, boost::bind(&AprilTagWrapper::testCallback, this), DEFAULT_IMAGE_SUB_PATH)
	, debug(this)
	, detection_time(20)
{
	nhp_.param(PACKAGE_PATH "family", config_.family_name_, std::string("tag36h11"));
	nhp_.param(PACKAGE_PATH "decimate", config_.quad_decimate_, 2.0);
	nhp_.param(PACKAGE_PATH "blur", config_.quad_sigma_, 0.0);
	nhp_.param(PACKAGE_PATH "threads", config_.nthreads_, 1);
	nhp_.param(PACKAGE_PATH "debug", config_.debug_, true);
	nhp_.param(PACKAGE_PATH "refine_edges", config_.refine_edges_, true);
	nhp_.param(PACKAGE_PATH "expected_frame_rate", config_.expected_frame_rate_hz_, 12.0);
	nhp_.param(PACKAGE_PATH "num_frames_for_test", config_.num_frames_for_test_, 30);

	config_.setFamily();
	td_ = apriltag_detector_create();
	config_.applyConfigToDetector(td_);
}

void AprilTagWrapper::end(void)
{
	std::string line = std::to_string(proc_width)
		+ "," + std::to_string(proc_height)
		+ "," + std::to_string(fps_stat.avg())
		+ "," + std::to_string(fps_stat.stdv())
		+ "," + std::to_string(detection_time.avg())
		+ "," + std::to_string(detection_time.stdv())
		+ "," + std::to_string(detection_stat.avg())
		+ "," + std::to_string(detection_stat.stdv())
		+ "," + std::to_string(config_.nthreads_);
	for (int i = 0; i < MAX_TARGETS; i++)
		line += "," + std::to_string(tracking_v_[i].avg());
	for (int i = 0; i < MAX_TARGETS; i++)
		line += "," + std::to_string(tracking_v_[i].stdv());
	debug.writeLine(line);

	debug.writeLine("FPS Avg.(Hz):,"
		+ std::to_string(fps_stat.avg())
		+ "\nFPS StDv.(Hz):,"
		+ std::to_string(fps_stat.stdv()),
		false);
	ros::shutdown();
}

void AprilTagWrapper::testCallback(void)
{
	image_sub_.getImageMat(display_frame_);
	if (is_ready(update_future))
	{
#ifdef CONSOLE_DEBUG
		debug.timesteps();
		debug.frameInfo();
		detection_manager_.print();
#endif

		processed_frames_++;
		double start_ms = nowMs();
		if (num_detections_ > 0)
		{
			detection_manager_.propagation_mgr_.updateROI(detection_manager_.subframe_offset_);
			detection_manager_.propagation_mgr_.propagate(detection_manager_.propagation_mgr_.q_sample_, display_frame_);
		}
#ifdef CONSOLE_DEBUG
		timesteps_["Propagation"].push_back({ msSince(start_ms), "ms" });
#endif
		cv::Mat subframe = detection_manager_.propagation_mgr_.getROIFromMat(display_frame_);
		cv::cvtColor(subframe, frameMat_, CV_BGR2GRAY);

#ifdef USE_WORKER_THREAD
		update_future = std::async(&AprilTagWrapper::update, this);
#else
		update_future = std::async(fakeWorker);
		AprilTagWrapper::update();
#endif

#ifdef DISPLAY_FRAME
		debug.propagations(display_frame_);
		debug.detections(display_frame_);
		debug.display(display_frame_);
#endif
		if (config_.num_frames_for_test_ > 0 && processed_frames_ > config_.num_frames_for_test_ + 1)
			end();
	}
	else
	{
		ignored_frames_++;
		dropped_frames_ += floor((int)((config_.expected_frame_rate_hz_ / image_sub_.getFrameRateHz()) - 1.));
	}
#ifdef CONSOLE_DEBUG
	timesteps_["Transport time"].push_back({ image_sub_.getLastTransportMs(), "ms " + std::to_string(display_frame_.cols) + "x" + std::to_string(display_frame_.rows) + "px" });
	timesteps_["Transport rate"].push_back({ image_sub_.getFrameRateHz(), "Hz" });
#endif
}

void AprilTagWrapper::update(void)
{
	double start_ms, stamp_ms;

	//timesteps_["Frame width"].push_back({ display_frame_.cols, "px" });
	//timesteps_["Frame height"].push_back({ display_frame_.rows, "px" });
	//timesteps_["Decimation"].push_back({ config_.quad_decimate_, "px/px" });

	start_ms = nowMs();
	image_u8_t im = (image_u8_t){ frameMat_.cols, frameMat_.rows, frameMat_.cols, frameMat_.data };

	zarray_t* new_detections = apriltag_detector_detect(td_, &im);

	proc_width = 1 + (im.width - 1) / config_.quad_decimate_;
	proc_height = 1 + (im.height - 1) / config_.quad_decimate_;
	stamp_ms = msSince(start_ms);
#ifdef CONSOLE_DEBUG
	timesteps_["Detect"].push_back({stamp_ms, "ms " + std::to_string(proc_width) + "x" + std::to_string(proc_height) + "px" });
#endif
	detection_time.add(stamp_ms);
	printf(">%.4f\n", stamp_ms);

	start_ms = nowMs();
	num_detections_ = detection_manager_.update(new_detections);
	process_rate_hz_ = 1000. / (msSince(timestamp_));

#ifdef CONSOLE_DEBUG
	timesteps_["Frame post"].push_back({ msSince(start_ms), "ms" });
	timesteps_["Tags detected"].push_back({ num_detections_, "tags" });
	timesteps_["Frame rate"].push_back({ process_rate_hz_, "hz" });
#endif

	if (ticks_++ > 0)
	{ // Add to statistics only after first frame
		fps_stat.add(process_rate_hz_);
		detection_stat.add((double)num_detections_);
		for (int i = 0; i < num_detections_ && i < MAX_TARGETS; i++)
			tracking_v_[i].add(detection_manager_.getJitter(i));
	}
	else
	{
		detection_manager_.propagation_mgr_.initializeFrame(im.width, im.height);
	}
}

void AprilTagWrapper::AprilTagDetectorConfig::setFamily()
{
	const char* famname = family_name_.c_str();
	if (famname == "") return;
	if (!strcmp(famname, "tag36h11")) {
		tf_ = tag36h11_create();
	}
	else if (!strcmp(famname, "tag25h9")) {
		tf_ = tag25h9_create();
	}
	else if (!strcmp(famname, "tag16h5")) {
		tf_ = tag16h5_create();
	}
	else if (!strcmp(famname, "tagCircle21h7")) {
		tf_ = tagCircle21h7_create();
	}
	else if (!strcmp(famname, "tagCircle49h12")) {
		tf_ = tagCircle49h12_create();
	}
	else if (!strcmp(famname, "tagStandard41h12")) {
		tf_ = tagStandard41h12_create();
	}
	else if (!strcmp(famname, "tagStandard52h13")) {
		tf_ = tagStandard52h13_create();
	}
	else if (!strcmp(famname, "tagCustom48h12")) {
		tf_ = tagCustom48h12_create();
	}
	else {
		printf("Unrecognized tag family name. Use e.g. \"tag36h11\".\n");
		family_name_ = "";
	}
	printf("Setting apriltag family to \"%s\".\n", family_name_.c_str());
}
void AprilTagWrapper::AprilTagDetectorConfig::applyConfigToDetector(apriltag_detector_t* td)
{
	apriltag_detector_add_family(td, tf_);
	td->quad_decimate = quad_decimate_;
	td->quad_sigma = quad_sigma_;
	td->nthreads = nthreads_;
	td->debug = debug_;
	td->refine_edges = refine_edges_;
}
std::string AprilTagWrapper::AprilTagDetectorConfig::ToString() const
{
	return std::to_string((int)(quad_decimate_ * 10))
		+ "_" + std::to_string((int)(quad_sigma_ * 100))
		+ "_" + std::to_string(nthreads_)
		+ "_" + std::to_string(refine_edges_);
}

AprilTagWrapper::Debug::Debug(AprilTagWrapper* host)
	: host_(host)
{
	bool file_exists = false;
	std::ifstream check_file(DEFAULT_DEBUG_FILE_NAME);
	file_exists = check_file.good();

	perffile.open(DEFAULT_DEBUG_FILE_NAME, std::fstream::out | std::fstream::app);
	if (!file_exists)
	{
		perffile << "Width(px),Height(px),FPS Avg.(Hz),FPS StDv.(Hz), Det. Avg.(ms), Det. StDv.(ms), Det. Avg.(tags),Det. StDv.(tags),Threads";
		for (int i = 0; i < MAX_TARGETS; i++) perffile << "," << std::to_string(i) << " Avg.";
		for (int i = 0; i < MAX_TARGETS; i++) perffile << "," << std::to_string(i) << " StDv.";
		perffile << std::endl;
	}
}

AprilTagWrapper::Debug::~Debug()
{
	if (outfile.is_open())
		outfile.close();
	if (perffile.is_open())
		perffile.close();
}

void AprilTagWrapper::Debug::frameInfo(void)
{
	std::cout << std::string(40, '=') << std::endl;
	std::cout << "Dropped frames: " << host_->dropped_frames_ << std::endl;
	std::cout << "Ignored frames: " << host_->ignored_frames_ << std::endl;
	std::cout << "Processed frames: " << host_->processed_frames_ << std::endl;
}

apriltag_detection_t AprilTagWrapper::AprilTagDetectionManager::offsetDetection(apriltag_detection_t* det) const
{
	apriltag_detection_t offset_det = { det->family, det->id, det->hamming, det->decision_margin, det->H };
	//printf("c:(%d, %d) | o:(%d, %d)\n", (int)det->c[0], (int)det->c[1], (int)subframe_offset_.x, (int)subframe_offset_.y);

	offset_det.c[0] = det->c[0] + subframe_offset_.x;
	offset_det.c[1] = det->c[1] + subframe_offset_.y;

	for (int i = 0; i < 4; i++)
	{
		offset_det.p[i][0] = det->p[i][0] + subframe_offset_.x;
		offset_det.p[i][1] = det->p[i][1] + subframe_offset_.y;
	}
	return offset_det;
}

int AprilTagWrapper::AprilTagDetectionManager::update(zarray_t* new_detections)
{
	if (new_detections == nullptr) return 0;

	detections_ = new_detections;

	int num_detections = zarray_size(detections_);
	//printf("N: %d\n", num_detections);
	if (num_detections < MIN_TARGETS)
	{
		propagation_mgr_.roi_manager_.reset(subframe_offset_);
		return 0;
	}
	propagation_mgr_.focal_frame_.resetPoints();
	std::vector<int> matched_v = {};

	bool found_match;
	int already_tracked = tracked_detections_.size();

	apriltag_detection_t* det;
	for (int i = 0; i < num_detections; i++) {
		zarray_get(detections_, i, &det);
		if (&det == nullptr) continue;

		apriltag_detection_t offset_det = offsetDetection(det);

		found_match = false;
		if (i < already_tracked)
		{
			int best_index = 0, i = 0;
			double best_match = -1, curr_match = max_transition_delta_;
			for (auto& check : tracked_detections_)
			{
				curr_match = compareDetections(&offset_det, &check.apriltag);
				//printf("Compare: %.3f(%d)\n", curr_match, i);
				if (best_match < 0 || curr_match < best_match)
				{
					best_match = curr_match;
					best_index = i;
				}
				i++;
			}
			//printf("Best: %.3f(%d)\n", best_match, best_index);
			if (best_match < max_transition_delta_)
			{
				if (std::find(matched_v.begin(), matched_v.end(), best_index) == matched_v.end())
				{
					found_match = true;
					detection_t update_det(offset_det, best_match);
					//printf("Update detection: W%d H%d\n", (int)update_det.width, (int)update_det.height);
					tracked_detections_[best_index] = update_det;
					matched_v.push_back(best_index);
				}
			}
		}

		//printf("Match %sfound\n", found_match ? "" : "not ");
		if (!found_match)
		{
			matched_v.push_back(tracked_detections_.size());
			detection_t new_det(offset_det, max_transition_delta_);
			tracked_detections_.push_back(new_det);
		}
	}

	detection_v new_tracked_detections_;
	//printf("Num points: %d\n", (int)propagation_mgr_.focal_frame_.numPoints());
	for (int i : matched_v)
	{
		new_tracked_detections_.push_back(tracked_detections_[i]);

		point_t p = getCenter(i);
		if (i >= propagation_mgr_.focal_frame_.numPoints())
			propagation_mgr_.focal_frame_.addPoint(p);
		else
			propagation_mgr_.focal_frame_.updatePoint(i, p);
	}

	tracked_detections_ = new_tracked_detections_;

	std::sort(tracked_detections_.begin(), tracked_detections_.end(), detectionSort);
	return matched_v.size();
}

double AprilTagWrapper::AprilTagDetectionManager::distance(double a[2], double b[2])
{
	double xd = a[0] - b[0];
	double yd = a[1] - b[1];

	return sqrt(xd * xd + yd * yd);
}

double AprilTagWrapper::AprilTagDetectionManager::compareDetections(apriltag_detection_t* a, apriltag_detection_t* b)
{
	return distance(a->c, b->c);
}

double AprilTagWrapper::AprilTagDetectionManager::getJitter(int i) const
{
	if (i >= tracked_detections_.size() || i < 0) return -1.;
	return tracked_detections_[i].jitter;
}

point_t AprilTagWrapper::AprilTagDetectionManager::getCenter(int i) const
{
	if (i >= tracked_detections_.size() || i < 0) return (point_t) { 0. };
	return (point_t) { tracked_detections_[i].apriltag.c[0], tracked_detections_[i].apriltag.c[1], tracked_detections_[i].width, tracked_detections_[i].height };
}

void AprilTagWrapper::AprilTagDetectionManager::print(void)
{
	int i = 0;
	printf("Jitter:");
	if (tracked_detections_.empty())
	{
		printf(" No targets.\n");
		return;
	}
	for (int i = 0; i < tracked_detections_.size(); i++) printf(" %6d", i);
	printf("\n       ");
	for (auto& d : tracked_detections_) printf(" %6.3f", d.jitter);
	printf("\n");
}

void AprilTagWrapper::Debug::timesteps(void)
{
	std::cout << std::string(40, '=') << std::endl;
	for (auto p : host_->timesteps_)
	{
		std::cout << p.first << ": " << p.second.back().first << p.second.back().second << std::endl;
	}

	try
	{
		if (host_->timesteps_.at("Frame width").size() <= 1) return;
		std::string file_name(DEFAULT_DEBUG_FILE_NAME);
		std::string csv_str(".csv");
		size_t start_pos = file_name.find(csv_str);
		if (start_pos == std::string::npos) return;

		int width = host_->timesteps_.at("Frame width").back().first;
		int height = host_->timesteps_.at("Frame height").back().first;
		int decimation = host_->timesteps_.at("Decimation").back().first;
		std::string config_str(host_->getConfigString());
		std::string id_str = "_" + std::to_string(width) + "x" + std::to_string(height) + "_" + config_str + csv_str;
		file_name.replace(start_pos, csv_str.length(), id_str);
		printf("File name: %s", file_name.c_str());
		if (file_name != outfile_name)
		{
			bool file_exists = false;
			std::ifstream check_file(file_name.c_str());
			file_exists = check_file.good();

			if (outfile.is_open())
				outfile.close();
			outfile.open(file_name.c_str(), std::fstream::out | std::fstream::app);


			if (!outfile.is_open())
				std::cout << "Debug file " << outfile_name << " failed to open!" << std::endl;
			else
			{
				outfile_name = file_name;
				std::cout << "Debug file " << outfile_name << " opened." << std::endl;
				if (!file_exists)
				{
					outfile << "Detect(ms),Frame rate(Hz),Frame post(ms),Transport rate(Hz),Transport time(ms)";
					for (int i = 0; i < MAX_TARGETS; i++) outfile << "," << std::to_string(i) << " Jitter(px)";
					//for (int i = 0; i < MAX_TARGETS; i++) outfile << "," << std::to_string(i) << " Avg.(px)";
					outfile << std::endl;
				}
			}
		}
	}
	catch (...) {
		printf("file open error\n");
	}

	if (outfile.is_open())
	{
		try
		{
			outfile << host_->timesteps_.at("Detect").back().first;
			outfile << "," << host_->timesteps_.at("Frame rate").back().first;
			outfile << "," << host_->timesteps_.at("Frame post").back().first;
			outfile << "," << host_->timesteps_.at("Transport rate").back().first;
			outfile << "," << host_->timesteps_.at("Transport time").back().first;
			for (int i = 0; i < MAX_TARGETS; i++) outfile << "," << std::to_string(host_->detection_manager_.getJitter(i));
			//for (int i = 0; i < MAX_TARGETS; i++) outfile << "," << std::to_string(host_->tracking_v_[i].avg());
			outfile << std::endl;
		}
		catch (...) {
			printf("file write error\n");
		}
	}
}
void AprilTagWrapper::Debug::detections(cv::Mat& frame)
{
	detection_v detections = host_->detection_manager_.get();


	int i = 0;
	while( i < detections.size() )
	{
		if (i >= MAX_TARGETS) break;
		apriltag_detection_t offset_det = detections[i].apriltag;

		//apriltag_detection_t offset_det = host_->detection_manager_.offsetDetection(det);
		cv::line(frame, cv::Point(offset_det.p[0][0], offset_det.p[0][1]),
			cv::Point(offset_det.p[1][0], offset_det.p[1][1]),
			cv::Scalar(0, 0xff, 0), 2);
		cv::line(frame, cv::Point(offset_det.p[0][0], offset_det.p[0][1]),
			cv::Point(offset_det.p[3][0], offset_det.p[3][1]),
			cv::Scalar(0, 0, 0xff), 2);
		cv::line(frame, cv::Point(offset_det.p[1][0], offset_det.p[1][1]),
			cv::Point(offset_det.p[2][0], offset_det.p[2][1]),
			cv::Scalar(0xff, 0, 0), 2);
		cv::line(frame, cv::Point(offset_det.p[2][0], offset_det.p[2][1]),
			cv::Point(offset_det.p[3][0], offset_det.p[3][1]),
			cv::Scalar(0xff, 0, 0), 2);

		std::stringstream ss;
		ss << offset_det.id;
		int fontface = CV_FONT_HERSHEY_SCRIPT_SIMPLEX;
		double fontscale = 0.5;
		int baseline;
		cv::Size textsize = cv::getTextSize(ss.str(), fontface, fontscale, 1,
			&baseline);
		cv::putText(frame, ss.str(), cv::Point(offset_det.p[3][0],
			offset_det.p[3][1] + 2 + textsize.height),
			fontface, fontscale, cv::Scalar(0xff, 0x99, 0), 2);

		i++;
	}
}

void AprilTagWrapper::Debug::propagations(cv::Mat& frame)
{
	host_->detection_manager_.propagation_mgr_.drawPointsOnCVMat(frame);
}

void AprilTagWrapper::Debug::display(cv::Mat& frame)
{
	if (frame.cols == 0) return;
	cv::Mat M;
	cv::resize(frame, M, cv::Size(DEFAULT_DISPLAY_WIDTH, DEFAULT_DISPLAY_HEIGHT));
	cv::imshow("Frame", M);
	cv::waitKey(30);
}

void AprilTagWrapper::Debug::writeLine(std::string line, bool perf)
{
	if (perf) {
		if (perffile.is_open())
			perffile << line << std::endl;
	}
	else {
		if (outfile.is_open())
			outfile << line << std::endl;
	}
}