#include "ROIManager.h"


ROIManager::ROIManager(uint32_t width, uint32_t height)
	: frame_size_(width, height)
{

}

void ROIManager::initialize(uint32_t width, uint32_t height)
{
	if (width == 0 || height == 0) return;
	frame_size_.width = width;
	frame_size_.height = height;
	initialized = true;
}

void ROIManager::reset(cv::Point& offset)
{
	//printf("Reset time is: %.4f\n", reset_timer_);
	printf("ROI: (%d, %d)[%d, %d] >", roi_.x, roi_.y, roi_.width, roi_.height);
	if (reset_timer_ == 0)
		reset_timer_ = nowMs();

	if (isTimedOut(reset_timer_, MAX_RESET_TRY_MS) || roi_.width == 0 || roi_.height == 0)
	{ // Give up after timeout and reset to full frame
		roi_.x = 0;
		roi_.y = 0;
		roi_.width = frame_size_.width;
		roi_.height = frame_size_.height;
		offset.x = 0;
		offset.y = 0;
		return;
	}

	double x_f = (frame_size_.width - roi_.width) * ROI_RESET_FACTOR;
	double y_f = (frame_size_.height - roi_.height) * ROI_RESET_FACTOR;
	roi_.x -= x_f;
	roi_.y -= y_f;
	roi_.width += 2 * x_f;
	roi_.height += 2 * y_f;

	roi_.x += last_i_;
	roi_.y += last_j_;

	limitBoundaryDimension(roi_.x, roi_.width, frame_size_.width);
	limitBoundaryDimension(roi_.y, roi_.height, frame_size_.height);

	printf(" (%d, %d)[%d, %d]\n", roi_.x, roi_.y, roi_.width, roi_.height);
	
	offset.x -= x_f;
	offset.y -= y_f;
}

void ROIManager::limitBoundaryDimension(int& x, int& w, int W)
{
	if (x < 0)
	{
		w += x;
		x = 0;
	}
	if (x > W)
	{
		x = W-1;
		w = 0;
	}
	else if (x + w > W)
	{
		w -= x + w - W;
	}

	if (w <= 0)
		w = W - x;
}

void ROIManager::update(cv::Rect& point_limits, cv::Point& offset)
{
	if (point_limits.width == 0 || point_limits.height == 0) return;
	//printf("B:(%d, %d)[%d, %d]\n", point_limits.x, point_limits.y, point_limits.width, point_limits.height);

	prev_roi_ = roi_;

	roi_.width = point_limits.width + ROI_PADDING_FACTOR;// *(1 + ROI_PADDING_FACTOR);
	roi_.height = point_limits.height + ROI_PADDING_FACTOR;// *(1 + ROI_PADDING_FACTOR);

	roi_.x = point_limits.x - (roi_.width - point_limits.width) / 2;
	roi_.y = point_limits.y - (roi_.height - point_limits.height) / 2;

	limitBoundaryDimension(roi_.x, roi_.width, frame_size_.width);
	limitBoundaryDimension(roi_.y, roi_.height, frame_size_.height);

	offset.x = roi_.x;
	offset.y = roi_.y;
	//printf("O:(%d, %d)\n", offset.x, offset.y);

	//printf("w%d h%d\n", roi_.x + roi_.width, roi_.y + roi_.height);
	//printf("Resetting reset timer.\n");
	reset_timer_ = 0;
}
